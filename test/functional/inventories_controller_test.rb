require 'test_helper'

class InventoriesControllerTest < ActionController::TestCase
  setup do
    @inventory = inventories(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:inventories)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create inventory" do
    assert_difference('Inventory.count') do
      post :create, inventory: { asset_custodian: @inventory.asset_custodian, asset_name: @inventory.asset_name, at_origin: @inventory.at_origin, availability: @inventory.availability, classification: @inventory.classification, data_retention_period: @inventory.data_retention_period, des_asset: @inventory.des_asset, info_moved_des: @inventory.info_moved_des, integrity: @inventory.integrity, op_function: @inventory.op_function, personal_data: @inventory.personal_data, personal_sensitive_data: @inventory.personal_sensitive_data, pro_name: @inventory.pro_name, pro_owner: @inventory.pro_owner, sensitive_customer_data: @inventory.sensitive_customer_data, type_asset: @inventory.type_asset }
    end

    assert_redirected_to inventory_path(assigns(:inventory))
  end

  test "should show inventory" do
    get :show, id: @inventory
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @inventory
    assert_response :success
  end

  test "should update inventory" do
    put :update, id: @inventory, inventory: { asset_custodian: @inventory.asset_custodian, asset_name: @inventory.asset_name, at_origin: @inventory.at_origin, availability: @inventory.availability, classification: @inventory.classification, data_retention_period: @inventory.data_retention_period, des_asset: @inventory.des_asset, info_moved_des: @inventory.info_moved_des, integrity: @inventory.integrity, op_function: @inventory.op_function, personal_data: @inventory.personal_data, personal_sensitive_data: @inventory.personal_sensitive_data, pro_name: @inventory.pro_name, pro_owner: @inventory.pro_owner, sensitive_customer_data: @inventory.sensitive_customer_data, type_asset: @inventory.type_asset }
    assert_redirected_to inventory_path(assigns(:inventory))
  end

  test "should destroy inventory" do
    assert_difference('Inventory.count', -1) do
      delete :destroy, id: @inventory
    end

    assert_redirected_to inventories_path
  end
end

require 'test_helper'

class LabellingsControllerTest < ActionController::TestCase
  setup do
    @labelling = labellings(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:labellings)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create labelling" do
    assert_difference('Labelling.count') do
      post :create, labelling: { criteria: @labelling.criteria, name: @labelling.name }
    end

    assert_redirected_to labelling_path(assigns(:labelling))
  end

  test "should show labelling" do
    get :show, id: @labelling
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @labelling
    assert_response :success
  end

  test "should update labelling" do
    put :update, id: @labelling, labelling: { criteria: @labelling.criteria, name: @labelling.name }
    assert_redirected_to labelling_path(assigns(:labelling))
  end

  test "should destroy labelling" do
    assert_difference('Labelling.count', -1) do
      delete :destroy, id: @labelling
    end

    assert_redirected_to labellings_path
  end
end

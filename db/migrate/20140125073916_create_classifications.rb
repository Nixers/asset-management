class CreateClassifications < ActiveRecord::Migration
  def change
    create_table :classifications do |t|
      t.string :c_type
      t.string :name
      t.string :a_type
      t.string :criteria
      t.integer :value

      t.timestamps
    end
  end
end

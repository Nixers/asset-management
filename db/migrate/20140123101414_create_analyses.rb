class CreateAnalyses < ActiveRecord::Migration
  def change
    create_table :analyses do |t|
      t.string :name
      t.string :description
      t.string :status
      t.string :status_description
      t.string :compensating_controls

      t.timestamps
    end
  end
end

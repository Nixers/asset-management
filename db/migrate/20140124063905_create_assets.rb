class CreateAssets < ActiveRecord::Migration
  def change
    create_table :assets do |t|
      t.string :name
      t.string :related_bus
      t.string :label
      t.string :type
      t.string :liabilities
      t.string :owner
      t.string :guardian
      t.string :user
      t.string :classification
      t.string :container
      t.string :description
      t.integer :classification_id
      t.integer :labelling_id
      t.references :document
      t.references :user

      t.timestamps
    end
  end
end

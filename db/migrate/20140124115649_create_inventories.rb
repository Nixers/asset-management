class CreateInventories < ActiveRecord::Migration
  def change
    create_table :inventories do |t|
      t.string :op_function
      t.string :pro_name
      t.string :pro_owner
      t.string :asset_name
      t.string :des_asset
      t.string :type_asset
      t.string :personal_data
      t.string :personal_sensitive_data
      t.string :sensitive_customer_data
      t.string :classification
      t.string :integrity
      t.string :availability
      t.string :asset_custodian
      t.string :data_retention_period
      t.string :at_origin
      t.string :info_moved_des

      t.timestamps
    end
  end
end

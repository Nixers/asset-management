# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20140125073916) do

  create_table "analyses", :force => true do |t|
    t.string   "name"
    t.string   "description"
    t.string   "status"
    t.string   "status_description"
    t.string   "compensating_controls"
    t.datetime "created_at",            :null => false
    t.datetime "updated_at",            :null => false
  end

  create_table "assets", :force => true do |t|
    t.string   "name"
    t.string   "related_bus"
    t.string   "label"
    t.string   "type"
    t.string   "liabilities"
    t.string   "owner"
    t.string   "guardian"
    t.string   "user"
    t.string   "classification"
    t.string   "container"
    t.string   "description"
    t.integer  "classification_id"
    t.integer  "labelling_id"
    t.integer  "document_id"
    t.integer  "user_id"
    t.datetime "created_at",        :null => false
    t.datetime "updated_at",        :null => false
  end

  create_table "businessunits", :force => true do |t|
    t.string   "b_name"
    t.string   "b_site"
    t.string   "b_area"
    t.string   "b_city"
    t.string   "b_state"
    t.string   "b_country"
    t.string   "time_zone"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "classifications", :force => true do |t|
    t.string   "c_type"
    t.string   "name"
    t.string   "a_type"
    t.string   "criteria"
    t.integer  "value"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "dashboards", :force => true do |t|
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "documents", :force => true do |t|
    t.string   "title"
    t.string   "location"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "inventories", :force => true do |t|
    t.string   "op_function"
    t.string   "pro_name"
    t.string   "pro_owner"
    t.string   "asset_name"
    t.string   "des_asset"
    t.string   "type_asset"
    t.string   "personal_data"
    t.string   "personal_sensitive_data"
    t.string   "sensitive_customer_data"
    t.string   "classification"
    t.string   "integrity"
    t.string   "availability"
    t.string   "asset_custodian"
    t.string   "data_retention_period"
    t.string   "at_origin"
    t.string   "info_moved_des"
    t.datetime "created_at",              :null => false
    t.datetime "updated_at",              :null => false
  end

  create_table "labellings", :force => true do |t|
    t.string   "name"
    t.string   "criteria"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "policies", :force => true do |t|
    t.string   "p_name"
    t.string   "p_code"
    t.string   "p_scope"
    t.string   "p_objective"
    t.string   "p_description"
    t.integer  "businessunit_id"
    t.integer  "document_id"
    t.integer  "user_id"
    t.datetime "created_at",          :null => false
    t.datetime "updated_at",          :null => false
    t.string   "attach_file_name"
    t.string   "attach_content_type"
    t.integer  "attach_file_size"
    t.datetime "attach_updated_at"
  end

  create_table "users", :force => true do |t|
    t.string   "email",                  :default => "", :null => false
    t.string   "encrypted_password",     :default => "", :null => false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          :default => 0
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.string   "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string   "unconfirmed_email"
    t.datetime "created_at",                             :null => false
    t.datetime "updated_at",                             :null => false
  end

  add_index "users", ["email"], :name => "index_users_on_email", :unique => true
  add_index "users", ["reset_password_token"], :name => "index_users_on_reset_password_token", :unique => true

end

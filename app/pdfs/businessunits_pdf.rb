class BusinessunitsPdf < Prawn::Document

	def initialize( businessunit)
		super()
		 @businessunit =  businessunit
		stroke_horizontal_rule
		pad_top(30) {}
		text "Report",:align => :center, :size => 30, :style => :bold
		text "Classification Type: #{ @businessunit.b_name}", :size =>  10
		move_down(30)
		text "Asset: #{ @businessunit.b_site}", :size =>  10
		move_down(30)
		text "Type: #{ @businessunit.b_area}", :size =>  10
		move_down(30)
		text "Classification Criteria: #{ @businessunit.b_city}", :size =>  10
		move_down(30)
		text "Value: #{ @businessunit.b_state}", :size =>  10
		move_down(30)
	end
end